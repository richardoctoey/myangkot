from django.db import models
from django_mongodb_engine.contrib import MongoDBManager
from djangotoolbox.fields import *
from bson.objectid import ObjectId #untuk ObjectId pada mongodb
import itertools

# Create your models here.


JENIS_ANGKOT = (
	('BUS', 'BUS'),
	('METROMINI', 'METRO MINI'),
	('BUSTRANS', 'TRANS JAKARTA'),
	('ANGKOT', 'ANGKUTAN UMUM KECIL')
)

class Angkot(models.Model):
	nama_angkot = models.CharField(max_length=10)
	jenis_angkot = models.CharField(choices=JENIS_ANGKOT)
	jalan_yang_dilalui = ListField(EmbeddedModelField('Jalan'))  # data seperti linked list a->b->c->d
	objects = MongoDBManager()
	curr_jalan = 0

	def __unicode__(self):
		return self.nama_angkot or u''

	#def cari_jalan(self,start_point,end_point):
	#	jalan_yang_dilalui

	def reset(self):
		self.curr_jalan = 0

	def get_current_jalan(self):
		return self.jalan_yang_dilalui[self.curr_jalan]

	def get_jalan_laju(self,value):
		try:
			self.curr_jalan += value
			if self.curr_jalan < 0:
				raise IndexError("Index pada data tidak tersedia")

			return self.jalan_yang_dilalui[self.curr_jalan]
		except IndexError:
			self.curr_jalan -= value
			raise IndexError("Index pada data tidak tersedia")

	def get_next_jalan(self):
		return self.get_jalan_laju(1)

	def get_prev_jalan(self):
		return self.get_jalan_laju(-1)

	def find_first_angkot(self, _nama_jalan):
		return Angkot.objects.raw_query({'jalan_yang_dilalui.nama_jalan':_nama_jalan})


	def search_within(self, first_angkots, _nama_jalan):
		for angkot in first_angkots:
			list_angkot_terdekat = []
			for jalan in angkot.jalan_yang_dilalui:
				if jalan.nama_jalan == _nama_jalan:
					print 'Found ',_nama_jalan
					return

			list_angkot_terdekat.append(jalan.angkot_terdekat)
			for angkot_terdekat in itertools.chain.from_iterable(list_angkot_terdekat):
				next_angkots = Angkot.objects.raw_query({'_id':ObjectId(angkot_terdekat.id)})
				self.search_within(next_angkots,_nama_jalan)

		print 'Not Found!'


	def search_angkot(self, start_point, end_point):
		list_angkot = self.find_first_angkot(start_point)
		self.search_within(list_angkot, end_point)

	#def find_last_location(self, _list_angkot_pertama,_nama_jalan):
	#	return self.objects.raw_query({'id':{'$in':_list_angkot_pertama},'jalan_yang_dilalui.nama_jalan':_nama_jalan})



class Jalan(models.Model):
	nama_jalan = models.CharField(max_length=30)
	angkot_terdekat = ListField(EmbeddedModelField('Angkot'))
	objects = MongoDBManager()
	curr_item = 0;

	def __unicode__(self):
		return self.nama_jalan or u''